#!/bin/sh

# Remove apt packages which are no longer unnecessary and delete
# downloaded packages
apt -y autoremove --purge
apt clean

# Remove SSH keys and machine ID so they get generated on first boot
rm -f /etc/ssh/ssh_host_* \
      /etc/machine-id

# Remove temporary dummy bootsector and u-boot binary
rm -f /tmp/bootsector.bin \
      /boot/u-boot-sunxi-with-spl.bin
